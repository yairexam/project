<?php

namespace App;
use carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Formrequest;
use App\ChangeStatus;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone','email', 'password','department_id','position_id','lastlogin','lastlogout','securityq','securityanswer',
    ];

    public function formrequests(){
        return $this->hasMany('App\Formrequest');
    }

    public function department(){
        return $this->belongsTo('App\Department');
    }
    public function position(){
        return $this->belongsTo('App\Position');
    }
    public function roles(){
        return $this->belongsToMany('App\Role','userroles');
    }
    public function securityq(){
        return $this->belongsTo('App\Securityq');
    }
    public function isAdmin(){
        $roles = $this->roles;
        if(!isset($roles)) return false;
        foreach($roles as $role){
            if($role->id == '1') return true;
        }
        return false;
    }

    public function isCyber(){
        $roles = $this->roles;
        if(!isset($roles)) return false;
        foreach($roles as $role){
            if($role->id == '2') return true;
        }
        return false;
    }
    public function countUser(){

        $userlist = User::all();

        return count($userlist);
    }
    public function activeReqUser(){
        $formrequest = Formrequest::all();
        $userlist = User::all();
        $countActiveUser =0;
        foreach($userlist as $user){
        $user = $formrequest->where('user_id','=',$user->id);
        if(count($user)>=1){
        $countActiveUser =$countActiveUser + 1;
        }
        }
        return $countActiveUser;
    }
    public function activeUser(){
        $userlist = User::all();
        $countActiveUser =0;
        $now = carbon::now();
        $now = $now->subMonth();
        $user = $userlist->where('lastlogin','>',$now);
        return count($user);
    }
    public function isIt(){
        $roles = $this->roles;
        if(!isset($roles)) return false;
        foreach($roles as $role){
            if($role->id == '3') return true;
        }
        return false;
    }
    public function countIt(){
        $formrequest = Formrequest::where('status_id','=','1')->get() ;
        return (count($formrequest));
    }
    public function countCyber(){
        $formrequest = Formrequest::where('status_id','=','2')->get() ;
        return (count($formrequest));
    }
    public function countApp(){
        $formrequest = Formrequest::where('status_id','=','3')->get() ;
        return (count($formrequest));
    }
    public function hasrole(){
        $userroles = Userrole::all();
        foreach($userroles as $userrole){
            if($userrole->user_id == $this->id){
                return true;
            }
        }
        return false;
    }
    public function getrole(){
        $roles = $this->roles;
        if(!isset($roles)) return "אין הרשאות";
        foreach($roles as $role){
             return $role->name;
        }
    }
    public function updatingg()
    {
        $user = $this;
        $formrequests = Formrequest::where('updated_at','>',$user->lastlogout)->where('created_at','<',$user->lastlogin)->get();
        $formrequests = $formrequests->where('user_id','=', $user->id );
        return count($formrequests);
    }
    public function updatesCyber()
    {
        $user = $this;
        $formrequests = Formrequest::where('status_id','=','2')->get();
        if(count($formrequests)>0){
            return true;
        }
        return false;
    }
    public function updatesIt()
    {
        $user = $this;
        $formrequests = Formrequest::where('status_id','=','1')->get();
        if(count($formrequests)>0){
            return true;
        }
        return false;
    }
    public function updatesApp()
    {
        $user = $this;
        $formrequests = Formrequest::where('status_id','=','3')->get();
        if(count($formrequests)>0){
            return true;
        }
        return false;
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

}
