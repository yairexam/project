<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Change extends Model
{
    public function formrequests()
    {
        return $this->hasMany('App\Formrequest');
    }
}
