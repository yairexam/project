<?php

namespace App\Providers;
use App\Userrole;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::allows('admin', function ($user) {
            return $user->isAdmin();
        });

        Gate::define('add-role', function ($user) {
            $userroles = Userrole::all();
            foreach($userroles as $userrole){
                if($userrole->user_id == $user->id){
                    return false;
                }
            }
            return true;
        });
        Gate::define('change-status-cyber', function ($user) {
            return $user->isCyber();
        });
        Gate::define('change-status-it', function ($user) {
            return $user->isIt();
        });
        Gate::define('change-status-imp', function ($user) {
            return $user->isCyber();
        });
        Gate::define('change-premitions', function ($user) {
            return $user->isAdmin();
        });
        //
    }
}
