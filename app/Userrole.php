<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userrole extends Model
{
    protected $fillable = [
        'user_id', 'role_id',
    ];
    public function role(){
        return $this->belongsTo('App\Role');
    }
}
