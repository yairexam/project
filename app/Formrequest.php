<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formrequest extends Model
{
    protected $fillable = ['changetype','texta', 'textb', 'textc', 'textd' ,'user_id', 'status_id','rejected','influencessystem','othertype' ];

    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }
    public function status(){
        return $this->belongsTo('App\Status');
    }
    public function change(){
        return $this->belongsTo('App\Change','changetype');
    }
    public function fromwichstatus()
    {
        if($this->status_id == '4'){
            $formrequest = $this;
            $laststatus = ChangeStatus::where('form_id','=',$formrequest->id)->where('to','=','4')->first();
        return $laststatus->from;
        }
        else{
            return $this->status_id;
        }
    }
}
