<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Formrequest;
use App\Userrole;
use App\Role;
use App\User;
use App\Department;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use ArielMejiaDev\LarapexCharts\LarapexChart;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(5);
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('add-user');
            $departments = Department::all();
            return view('users.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $use = $user->create($request->all());
        $use->password =  Hash::make($request['password']);
        $use->save();
        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $departments = Department::all();
        $roles = Role::all();
        $userrole = Userrole::all();
        $role = $userrole->where('user_id','=',$id);
        return view('users.edit', compact('user','departments','roles','role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());

        if(Auth::user()->isAdmin()){
            if(isset($request->role_id)){
                $userrole = new Userrole();
                $userrole->user_id = $id;
                $userrole->role_id = $request->role_id;
                $userrole->save();
            }

          }
          return back();
    }
    public function deletepremition($id)
    {
        if(Auth::user()->isAdmin()){
            $userrole = Userrole::all();
            $userrole = $userrole->where('user_id','=',$id);
            foreach($userrole as $role){
                $role->delete();
            }
        }
        else{
                Session::flash('notallowed', 'אין לך הרשאות לפעולה זו');
            }
            return back();
          }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->isAdmin()){
        $user = User::findOrFail($id);
        $user->delete();
    }
    else{
            Session::flash('notallowed', 'אין לך הרשאות לפעולה זו');
        }
        return redirect('users');
    }
}
