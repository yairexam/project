<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Status;
use App\Change;
use carbon\carbon;
use App\Formrequest;
use App\ChangeStatus;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use ArielMejiaDev\LarapexCharts\LarapexChart;
use Mail;
use App\Mail\StatusChange;


class FormrequestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formrequests = Formrequest::all();
        $users = User::all();
        $statuses = Status::all();
        $changes = Change::all();
        return view('formrequests.index', compact('formrequests','users', 'statuses','changes'));
    }

    public function myRequests()
    {
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $formrequests = $user->formrequests;
        $users = User::all();
        $statuses = Status::all();
        return view('formrequests.index', compact('formrequests','users', 'statuses'));
    }

    public function managment()
    {
        if(Auth::user()->isAdmin()){
        $formrequests = Formrequest::paginate(5);
        $statuschange = changeStatus::all();
        $countIt=0;
        $countCyber=0;
        $countapp=0;
        $daysIt=0;
        $daysCyber=0;
        $daysapp=0;
        $lateIt=0;
        $lateCyber=0;
        $lateapp=0;
        foreach($formrequests as $formrequest){
            $statuschangeforms = $statuschange->where('form_id',"=",$formrequest->id);
            foreach($statuschangeforms as $statuschangeform){
                if($statuschangeform->from==1){
                    $to=new Carbon($statuschangeform->created_at);
                    $from=new Carbon($formrequest->created_at);
                    $countIt=$countIt+1;
                    $daysIt = $daysIt + ($to->diffInDays($from) );
                }elseif($statuschangeform->from==2){
                    $stat=changeStatus::where('form_id','=',$formrequest->id)->where('from','=','1')->where('to','=','2')->first();
                    if(isset($stat)){
                    $to=new Carbon($stat->created_at);
                    $from=new Carbon($statuschangeform->created_at);
                    $countCyber= $countCyber+1;
                    $daysCyber = $daysCyber +  ($from->diffInDays($to));
                }
                }elseif($statuschangeform->from==3){

                    $stat=$statuschange->where('form_id','=',$formrequest->id)->where('from','=','2')->where('to','=','3')->first();
                    if(isset($stat)){
                    $to=new Carbon( $stat->created_at);
                    $from=new Carbon($formrequest->created_at);
                    $countapp=$countapp+1;
                    $daysapp = $daysapp + ($from->diffInDays($to) );
                }
                }
        }
        }

        foreach($formrequests as $formrequest){
            $from=new Carbon($formrequest->created_at);
            $days= ($from->diffInDays(carbon::now()));
            if($days > 15){
                if($formrequest->status_id==1){
                    $lateIt=$lateIt+1;
                }elseif($formrequest->status_id==2){
                    $lateCyber=$lateCyber+1;
                }elseif($formrequest->status_id==3){
                    $lateapp=$lateapp+1;
                }
            }

        }

        $cyberformrequests =$daysCyber/max($countCyber,1);
        $itformrequests =$daysIt/ max($countIt,1);
        $appformrequests =$daysapp/ max($countapp,1);
    }
    else{
            Session::flash('notallowed', 'אין לך הרשאות לפעולה זו');
        }
        return view('formrequests.managment' ,compact('formrequests','cyberformrequests','itformrequests','appformrequests','lateapp','lateCyber','lateIt') );
    }


    public function multiplerecordsdelete(Request $req){
        $id=$req->id;
        foreach ($id as $ke) {
        DB::table('formrequests')->where('id','=', $ke)->delete();
    }
        return redirect()->back()->with('message','בחירה מרובה של טפסים נמחקה');
    }

    public function statusC(Request $request, $cid)
    {
        $formrequests = Formrequest::findOrFail($cid);
        if($formrequests->status_id !=$request->status_id ){
        if(($request->status_id == '2'|| $request->status_id == '4') & ($formrequests->status_id == '1') & (Auth::user()->isIt()))
        {
        $statuschange = New ChangeStatus();
        $statuschange->form_id = $cid;
        $statuschange->from = $formrequests->status_id;
        $statuschange->to = $request->status_id;
        $statuschange->details = $request->rejected;
        $statuschange->save();
        $formrequests->status_id = $request->status_id;
        $formrequests->save();
        $to_name =$formrequests->owner->name;
        $to_email = $formrequests->owner->email;
        $data = array('name'=>'Cloudways', 'body' => 'A test mail');
        Mail::to($formrequests->owner->email)->send(new StatusChange($formrequests));
         return back();
        }
        elseif(($formrequests->status_id > 1) & Auth::user()->isCyber()){
            $statuschange = New ChangeStatus();
            $statuschange->form_id = $cid;
            $statuschange->from = $formrequests->status_id;
            $statuschange->to = $request->status_id;
            $statuschange->details = $request->rejected;
            $statuschange->save();
            $formrequests->status_id = $request->status_id;
            $formrequests->save();
            $to_name =$formrequests->owner->name;
        $to_email = $formrequests->owner->email;
        $data = array('name'=>'Cloudways', 'body' => 'A test mail');
        Mail::to($formrequests->owner->email)->send(new StatusChange($formrequests));
            return back();
        }
        else{
            Session::flash('notallowed', 'You are not allowed to change the status of the request becuase you are not autorized for this kind of approval');
            return back();
        }
    }
        return back();
    }

    public function itRequests()
    {
        $formrequests = Formrequest::where('status_id','=','1')->get();;
        $users = User::all();
        $statuses = Status::all();
        $changes = Change::all();
        return view('formrequests.index', compact('formrequests','users', 'statuses','changes'));
    }
    public function deniedRequests()
    {
        if(auth::user()->isAdmin()||auth::user()->isCyber()||auth::user()->isIt()){
            $formrequests = Formrequest::where('status_id','=','4')->get();;
            $users = User::all();
            $statuses = Status::all();
            $changes = Change::all();
        return view('formrequests.index', compact('formrequests','users', 'statuses','changes'));
        }
        else{
            $formrequests = Formrequest::where('status_id','=','4')->where('user_id','=',Auth::id())->get();;
            $users = User::all();
            $statuses = Status::all();
            $changes = Change::all();
            return view('formrequests.index', compact('formrequests','users', 'statuses','changes'));

        }
    }

    public function updatedRequests()
    {
        $user = Auth::user();
        $formrequests = Formrequest::where('updated_at','>',$user->lastlogout)->get();
        $formrequests = $formrequests->where('user_id','=', $user->id );
        $users = User::all();
        $statuses = Status::all();
        $changes = Change::all();
        return view('formrequests.index', compact('formrequests','users', 'statuses','changes'));
    }

    public function aprovedRequests()
    {
        if(auth::user()->isAdmin()||auth::user()->isCyber()||auth::user()->isIt()){
        $formrequests = Formrequest::where('status_id','=','5')->get();;
        $users = User::all();
        $statuses = Status::all();
        $changes = Change::all();
        return view('formrequests.index', compact('formrequests','users', 'statuses','changes'));
        }
        else{
            $formrequests = Formrequest::where('status_id','=','5')->where('user_id','=',Auth::id())->get();;
            $users = User::all();
            $statuses = Status::all();
            $changes = Change::all();
            return view('formrequests.index', compact('formrequests','users', 'statuses','changes'));

        }
    }
    public function cyberRequests()
    {
        $formrequests = Formrequest::where('status_id','=','2')->get();;
        $users = User::all();
        $statuses = Status::all();
        $changes = Change::all();
        return view('formrequests.index', compact('formrequests','users', 'statuses','changes'));
    }


    public function appRequests()
    {
        $formrequests = Formrequest::where('status_id','=','3')->get();;
        $users = User::all();
        $statuses = Status::all();
        $changes = Change::all();
        return view('formrequests.index', compact('formrequests','users', 'statuses','changes'));
    }

    public function searchformbyid(Request $request)
    {
        $formrequests = Formrequest::where('id','=',$request->search)->get();;
        $users = User::all();
        $statuses = Status::all();
        $changes = Change::all();
        return view('formrequests.index', compact('formrequests','users', 'statuses','changes'));
    }
    public function searchformbystatus(Request $request)
    {
        $formrequests = Formrequest::where('status_id','=',$request->status_id)->get();;
        $users = User::all();
        $statuses = Status::all();
        $changes = Change::all();
        return view('formrequests.index', compact('formrequests','users', 'statuses','changes'));
    }
    public function searchformbychange(Request $request)
    {
        $formrequests = Formrequest::where('changetype','=',$request->seachange_id)->get();;
        $users = User::all();
        $statuses = Status::all();
        $changes = Change::all();
        return view('formrequests.index', compact('formrequests','users', 'statuses','changes'));
    }

    public function changeUser($cid, $uid = null){
        Gate::authorize('assign-user');
        $formrequests = Formrequest::findOrFail($cid);
        $formrequests->user_id = $uid;
        $formrequests->save();
        return back();
    }

    public function changeStatus($cid, $sid)
    {
        $formrequests = Formrequest::findOrFail($cid);
        if(Gate::allows('change-status', $formrequests))
        {
            $from = $formrequests->status->id;
            if(!Status::allowed($from,$sid)) return redirect('formrequests');
            $formrequests->status_id = $sid;
            $formrequests->save();
        }else{
            Session::flash('notallowed', 'You are not allowed to change the status of the user becuase you are not the owner of the user');
        }
        return back();
        //return redirect('candidates');
    }

    public function page($id)
    {
        $formrequests = Formrequest::findOrFail($id);
        $status = Status::all();
        $statuses = ChangeStatus::where('form_id','=',$id)->get();
        $It = $statuses->where('from','=',1)->first();
        if(isset($It)){
            $statusIt = $It->details;
        }
        else{
                $statusIt = "טרם הוזן";
        }

        $Cyber = $statuses->where('from','=',2)->first();
        if(isset($Cyber)){
        $statusCyber = $Cyber->details;
        }
        else{
            $statusCyber = "טרם הוזן";
        }
        $App = $statuses->where('from','=',2)->first();
        if(isset($App)){
            $statusApp = $App->details;
        }
        else{
                $statusApp = "טרם הוזן";
        }

        return view('formrequests.page', compact('formrequests','status','statusIt','statusCyber','statusApp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        foreach($users as $user){
            if($user->id === Auth::id()){
                $authuser = $user;
            }
        }
        $changes = Change::all();
        return view('formrequests.create', compact('authuser','changes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formrequests = new Formrequest();
        $req = $formrequests->create($request->all());
        $req->status_id = 1;
        $req->user_id = Auth::id();
        $req->save();
        return redirect()->route('formrequests.page',$req->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $formrequests = Formrequest::findOrFail($id);
        return view('formrequests.edit', compact('formrequests'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $formrequests = Formrequest::findOrFail($id);
       $formrequests->update($formrequests->all());
       return redirect('formrequests');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $formrequests = Formrequest::findOrFail($id);
        $formrequests->delete();
        return redirect('formrequests');
    }
}
