<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormrequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formrequests', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('changetype')->unsigned()->nullable()->index;
            $table->text('othertype')->nullable();
            $table->text('texta');
            $table->text('textb');
            $table->text('textc');
            $table->text('textd');
            $table->text('influencessystem')->nullable();
            $table->text('rejected')->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable()->index;
            $table->bigInteger('status_id')->nullable()->unsigned()->index;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formrequest');
    }
}
