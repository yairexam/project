<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            [
                'name' => 'מנהלה',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'משאבי מים וטבע',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'גאולוגיה הנדסית וסיכונים גאולוגים',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ] ,
            [
                'name' => 'סטרטיגרפיה וחקר תת הקרקע',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ] ,
            [
                'name' => 'גאוכימיה וגאולוגיה סביבתית',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]  ,
            [
                'name' => 'אגף מיפוי גאולוגי',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'ססמולוגיה',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
            ]);
    }
}
