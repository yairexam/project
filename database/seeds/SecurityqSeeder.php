<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class SecurityqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('securityq')->insert([
            [
                'question' => 'מה שם בית הספר היסודי בו למדת',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'question' => 'מה שם הנעורים של אמא שלך',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'question' => 'באיזה שנה נכנסת לשירות המדינה',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'question' => 'מה שם חיית המחמד האהובה עלייך',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            ]);
    }
}
