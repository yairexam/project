@extends('layouts.app')

@section('title', 'לוח מעקב')

@section('content')

@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
@if (Session::has('message'))
            <div class="alert alert-success">{{Session::get('message')}} </div>
@endif
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
            <div class="p-3 mb-2  text-dark">
                <h1 class="text-dark" style=" text-align:right "></h1>
            </div>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content" style = "direction: rtl">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-light">
              <div class="inner">
                <h3>{{Auth::user()->countIt()}}<sup style="font-size: 20px"></sup></h3>

                <p>בקשות ממתינות לאישור מנמ"ר</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>

              <a href="{{route('formrequests.itrequests')}}" class="small-box-footer bg-success" style="z-index:0;">ראה בקשות <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        <div class="col-lg-4 col-6">
          <!-- small box -->
          <div class="small-box bg-light">
            <div class="inner">
              <h3>{{Auth::user()->countCyber()}}</h3>

              <p>בקשות ממתינות לאישור סייבר</p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
            <a href="{{route('formrequests.cyberrequests')}}" class="small-box-footer bg-info" style="z-index:0;">ראה בקשות <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-4 col-6">
          <!-- small box -->
          <div class="small-box bg-light">
            <div class="inner">
              <h3>{{Auth::user()->countApp()}}</h3>

              <p>בקשות ממתינות ליישום</p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
            <a href="{{route('formrequests.apprequests')}}" class="small-box-footer bg-warning" style="z-index:0;">ראה בקשות <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="card">
                <!-- Morris chart - Sales -->
        </div>
          <!-- /.card -->



          <!-- TO DO List -->
          <div class="card bg-primary-gradient" style="text-align:right">
            <div class="card-header no-border">
              <h3 class="card-title">
                <i class="ion ion-clipboard mr-1"></i>
                בקשות במערכת
              </h3>

              <div class="card-tools">
                <ul class="pagination pagination-sm">
                  {{$formrequests->links()}}
                </ul>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body " >
            <form method="POST" action="{{route('formrequests.multiplerecordsdelete')}}">
                    @csrf
            <table class="table">
                <tr>
                    <td>:מס אסמכתא</td>
                    <td>סוג השינוי:</td>
                    <td>שם עובד:</td>
                    <td>סטטוס בקשה:</td>
                </tr>

                @foreach($formrequests as $formrequest)

                <tr>
                  <!-- drag handle -->

                  <!-- checkbox -->

                  <!-- todo text -->
                <td><a href = "{{route('formrequests.page',$formrequest->id)}}" >{{$formrequest->id}}</a></td>
                <td>{{$formrequest->change->name}}</td>
                <td>{{$formrequest->owner->name}}</td>
                <td>{{$formrequest->status->name}}</td>
                <td><input type="checkbox" value="{{$formrequest->id}}" name="id[]"></td>
                  {{-- <!-- General tools such as edit or delete-->
                  <div class="tools">
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-trash-o"></i>
                  </div> --}}
                </tr>
                @endforeach
              </table>
              <input type="submit" name="submit" value="מחק בחירה" class="btn btn-info float-right" />

             </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">

            </div>

          </div>
          <!-- /.card -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">

          <!-- Map card -->
          <div class="card bg-primary-gradient" style="text-align:right">
            <div class="card-header no-border" >
              <h3  style="text-align:center" >
                סייבר
              </h3>
            </div>
            <div class="card-body">
                <table class = "table table-light">
                    <!-- the table data -->
                        <tr>
                            <td>זמן ממוצע לטיפול בבקשות בימים:</td>
                            <td>{{$cyberformrequests}}</td>
                        </tr>
                        <tr>
                            <td>כמות בקשות ממתינות :</td>
                            <td>{{Auth::user()->countCyber()}} </td>
                        </tr>
                        <tr>
                            <td>כמות בקשות בחריגה :</td>
                            <td>{{$lateCyber}}</td>
                        </tr>
                </table>
            </div>
            <!-- /.card-body-->
            <div class="card-footer bg-transparent">
              <div class="row">
                <div class="col-4 text-center">
                  <div id="sparkline-1"></div>
                  <div class="text-white">Visitors</div>
                </div>
                <!-- ./col -->
                <div class="col-4 text-center">
                  <div id="sparkline-2"></div>
                  <div class="text-white">Online</div>
                </div>
                <!-- ./col -->
                <div class="col-4 text-center">
                  <div id="sparkline-3"></div>
                  <div class="text-white">Sales</div>
                </div>
                <!-- ./col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.card -->

            <!-- Map card -->
            <div class="card bg-primary-gradient" style="text-align:right">
              <div class="card-header no-border" >
                <h3  style="text-align:center" >
                  מנמ"ר
                </h3>
              </div>
              <div class="card-body">
                  <table class = "table table-light">
                      <!-- the table data -->
                          <tr>
                              <td>זמן ממוצע לטיפול בבקשה בימים:</td>
                              <td>{{$itformrequests}}</td>
                          </tr>
                          <tr>
                              <td>כמות בקשות ממתינות :</td>
                              <td>{{Auth::user()->countIt()}} </td>
                          </tr>
                          <tr>
                              <td>כמות בקשות בחריגה :</td>
                              <td>{{$lateIt}}</td>
                          </tr>
                  </table>
              </div>
              <!-- /.card-body-->
              <div class="card-footer bg-transparent">
                <div class="row">
                  <div class="col-4 text-center">
                    <div id="sparkline-1"></div>
                    <div class="text-white">Visitors</div>
                  </div>
                  <!-- ./col -->
                  <div class="col-4 text-center">
                    <div id="sparkline-2"></div>
                    <div class="text-white">Online</div>
                  </div>
                  <!-- ./col -->
                  <div class="col-4 text-center">
                    <div id="sparkline-3"></div>
                    <div class="text-white">Sales</div>
                  </div>
                  <!-- ./col -->
                </div>
                <!-- /.row -->
              </div>
            </div>
            <!-- /.card -->

                <!-- Map card -->
                <div class="card bg-primary-gradient" style="text-align:right">
                  <div class="card-header no-border" >
                    <h3  style="text-align:center" >
                      יישום
                    </h3>
                  </div>
                  <div class="card-body">
                      <table class = "table table-light">
                          <!-- the table data -->
                              <tr>
                                  <td>זמן ממוצע לטיפול בבקשה בימים :</td>
                                  <td>{{$appformrequests}}</td>
                              </tr>
                              <tr>
                                  <td>כמות בקשות ממתינות :</td>
                                  <td>{{Auth::user()->countapp()}} </td>
                              </tr>
                              <tr>
                                  <td>כמות בקשות בחריגה :</td>
                                  <td>{{$lateapp}}</td>
                              </tr>
                      </table>
                  </div>
                  <!-- /.card-body-->
                  <div class="card-footer bg-transparent">
                    <div class="row">
                      <div class="col-4 text-center">
                        <div id="sparkline-1"></div>
                        <div class="text-white">Visitors</div>
                      </div>
                      <!-- ./col -->
                      <div class="col-4 text-center">
                        <div id="sparkline-2"></div>
                        <div class="text-white">Online</div>
                      </div>
                      <!-- ./col -->
                      <div class="col-4 text-center">
                        <div id="sparkline-3"></div>
                        <div class="text-white">Sales</div>
                      </div>
                      <!-- ./col -->
                    </div>
                    <!-- /.row -->
                  </div>
                </div>
                <!-- /.card -->



          </div>
          <!-- /.card -->

        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->



@endsection

