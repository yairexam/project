@extends('layouts.app')

@section('title', 'בקשות שינויים')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<div class="container" style="direction:rtl;">
    <div class="background" style=" background: rgba(255, 255, 255, 0.85);"><p>

@if(Route::currentRouteName()=='formrequests.myrequests')
<h1 style="text-align:center">טפסי בקשות השינויים שלי</h1>
@elseif(Route::currentRouteName()=='formrequests.itrequests')
<h1 style="text-align:center">טפסי בקשות השינויים הממתינים לאישור מנמ"ר</h1>

@elseif(Route::currentRouteName()=='formrequests.cyberrequests')
<h1 style="text-align:center">טפסי בקשות השינויים הממתינים לאישור סייבר</h1>
@elseif(Route::currentRouteName()=='formrequests.apprequests')
<h1 style="text-align:center">טפסי בקשות השינויים הממתינים ליישום</h1>
@elseif(Route::currentRouteName()=='formrequests.aprovedrequests')
<h1 style="text-align:center">טפסי בקשות השינויים שאושרו</h1>
@elseif(Route::currentRouteName()=='formrequests.deniderequests')
<h1 style="text-align:center">טפסי בקשות השינויים שנדחו</h1>
@else
<h1 style="text-align:center">טפסי בקשות השינויים במערכות מידע</h1>
@endif
</p></div>
@if(Auth::user()->isAdmin() && Route::currentRouteName()=='formrequests.index')
    <div class="background" style=" background: rgba(255, 255, 255, 0.85);">

    <form class="form-inline ml-3" method = "post" action = "{{action('FormrequestsController@searchformbychange')}}">
        @csrf
        <div class="input-group input-group-sm">
            <div class="form-group">
                <label for="department_id" class="col-md-3 col-form-label text-md-right">סוג שינוי: </label>
                <div class="col-md-4">
                    <select class="form-control" name="change_id">
                    @foreach ($changes as $change)
                        <option value="{{ $change->id }}">
                            {{ $change->name }}
                        </option>
                    @endforeach
                    </select>
                </div>
            </div>
        <div class="input-group-append">
            <button class="btn btn-navbar" type="submit" >
            <i class="fas fa-search"></i>
            </button>
        </div>
        </div>
    </form>
    <form class="form-inline ml-3" method = "post" action = "{{action('FormrequestsController@searchformbystatus')}}">
        @csrf
        <div class="input-group input-group-sm">
            <div class="form-group">
                <label for="department_id" class="col-md-3 col-form-label text-md-right">סטטוס: </label>
                <div class="col-md-4">
                    <select class="form-control" name="status_id">
                    @foreach ($statuses as $status)
                        <option value="{{ $status->id }}">
                            {{ $status->name }}
                        </option>
                    @endforeach
                    </select>
                </div>
            </div>
        <div class="input-group-append">
            <button class="btn btn-navbar" type="submit" >
            <i class="fas fa-search"></i>
            </button>
        </div>
        </div>
    </form>
    </div>
@endif
</p>


<div class="row row-cols-1 row-cols-md-3"  >
    @if(count($formrequests)==0)
    <div class="card text-center">
        <div class="card-body ">
            <p class="card-text-center"><b>לא נמצאו בקשות להצגה ! </b></p>
        </div>
        </div>
    @else
    @foreach($formrequests as $formrequest)
    <div class="col mb-4">

        <div class="card text-center">

        @if($formrequest->status->id == 4)
        <div class="card-header text-white bg-danger text-center">מספר אסמכתא לבקשה: {{$formrequest->id}}
        @elseif($formrequest->status->id == 5)
        <div class="card-header  text-white bg-info text-center">מספר אסמכתא לבקשה: {{$formrequest->id}}
        @else
        <div class="card-header "><b>מספר אסמכתא לבקשה:</b> {{$formrequest->id}}
        @endif
        </div>

        <div class="card-body ">
            @if($formrequest->status->id == 1)
            <img src="{{url('img/statusone.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
            @elseif($formrequest->status->id == 2)
            <img src="{{url('img/statustwo.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
            @elseif($formrequest->status->id == 3)
            <img src="{{url('img/statusthree.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
            @elseif($formrequest->status->id == 4)
                @if($formrequest->fromwichstatus() == 1)
                <img src="{{url('img/statusonedenied.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
                @elseif($formrequest->fromwichstatus() == 2)
                <img src="{{url('img/statustwodenied.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
                @elseif($formrequest->fromwichstatus() == 3)
                <img src="{{url('img/statusthreedenied.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
                @endif
            @elseif($formrequest->status->id == 5)
            <img src="{{url('img/applied.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
            @endif
            <p class="card-text"><b>שם המבקש: </b></p>
            <p class="card-text">{{$formrequest->owner->name}}</p>
            <p class="card-text"> <b>תאריך הגשת הבקשה: </b></p>
            <p class="card-text">{{$formrequest->created_at}}</p>
            <p class="card-text"><b>סטטוס הבקשה:</b> </p>
            <p class="card-text">{{$formrequest->status->name}}</p>
            <p class="card-text"><b>סוג שינוי מבוקש:</b></p>
            <p class="card-text">{{$formrequest->change->name}}</p>
            @if(isset($formrequest->rejected))
            <p class="card-text"><b>סיבת דחייה: </b></p>
            <p class="card-text">{{$formrequest->rejected}}</p>
            @endif

            <p class="card-text">
                @if($formrequest->owner->id== Auth::user()->id)
                <a href="{{route('formrequests.delete',$formrequest->id)}}"><button type="button" class="btn btn-outline-primary" >מחק בקשה </button></a>
                    @endif
                </p>
            <p class="card-text">
                <a href= "{{route('formrequests.page',$formrequest->id)}}"><button type="button" class="btn btn-outline-primary" >פרטים נוספים</button> </a>
        </p>

        </div>
        <div class="card-footer">
            <small class="card-text">עודכן לאחרונה: {{$formrequest->updated_at}}</small>
          </div>
      </div>
    </div>
    @endforeach
    @endif
</div>
</div>
  @endsection
