@extends('layouts.app')

@section('title', 'טופס בקשה לשינוי')

@section('content')
<p></p>
           <div class="container contact" style="text-align:right; background: rgba(255, 255, 255, 0.8); direction: rtl;">
            <div class="row">
                <div class="col-md-3">
                    <div class="contact-info">
                        <img src="{{url('img/logo.jpg')}}" alt="image"/>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="contact-form">
                        <h1>יצירת טופס בקשה לשינוי</h1>
                        <label for = "name"> שם מגיש הבקשה:    {{$authuser->name}}   </label><p>
                        <label for = "name"> אגף:    {{$authuser->department->name}}   </label><p>
                        <label for = "name"> מעמד:    {{$authuser->position->name}}   </label><p>
                        <label for = "name"> טלפון:    {{$authuser->phone}}   </label><p>

                        <form method = "post" action = "{{action('FormrequestsController@store')}}">
                        @csrf
                        <label  for="changetype" class="col-md-4 col-form-label text-right">סוג השינוי</label>
                        <div class="col-md-6">
                            <select class="form-control" name="changetype"  >
                               @foreach ($changes as $change)
                                 <option value="{{ $change->id }}">
                                     {{ $change->name }}
                                 </option>
                               @endforeach
                             </select>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-2" for="othertype">אחר?:</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" name="othertype" placeholder="פרט">
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="texta">תיאור בקשת השינוי כולל מטרה וסיבת הבקשה:</label>
                          <div class="col-sm-10">
                            <textarea  class="form-control" rows="5" name="texta" placeholder="" required></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="textb" >האם יש צורך בתיעדוף השינוי בנסיבות מיוחדות אנא פרט:</label>
                          <div class="col-sm-10">
                            <textarea  class="form-control" rows="5" name="textb" placeholder="" required></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="textc">האם מדובר בשינוי חוזר או ידוע על שינוי דומה?</label>
                          <div class="col-sm-10">
                            <textarea  class="form-control" rows="5" name="textc" placeholder="" required></textarea>
                          </div>
                        </div>

                        <div class="form-group">
                            <label for="textd">האם ידוע לך על מערכות אחרות שיושפעו מתהליך השינוי?</label>
                          <div class="col-sm-10">
                            <select class="form-control" name="textd"  >
                                <option value="כן">כן</option>
                                <option value="לא">לא</option>
                            </select>
                          </div>
                        </div>

                        <div class="form-group">
                            <label for="influencessystem"> פרט:</label>
                          <div class="col-sm-10">
                            <textarea  class="form-control" rows="5" name="influencessystem" placeholder="" required></textarea>
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <input class="btn btn-primary"  type = "submit" name = "submit" value = "צור טופס בקשה">
                          </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
    </div>
@endsection
