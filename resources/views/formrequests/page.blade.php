@extends('layouts.app')

@section('title', 'טופס')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<div class="container " style="direction: rtl;" >
    <div class="background" style=" background: rgba(255, 255, 255, 0.85);"><p>
        <h1 style="text-align:center">פרטי טופס בקשה</h1>
        <div style="text-align:center"><a href =  "{{url('/formrequests')}}" > חזרה לרשימת הטפסים שלי</a></div></p></div>
    <div class="card text-center ">
    <div class="card-header" style="text-align:center">    <b>  מספר אסמכתא לבקשה:            </b>      {{$formrequests->id}}
    </div>
    <div class="card-body">
    <!-- the table data -->
    @if($formrequests->status->id == 1)
    <img src="{{url('img/statusone.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
    @elseif($formrequests->status->id == 2)
    <img src="{{url('img/statustwo.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
    @elseif($formrequests->status->id == 3)
    <img src="{{url('img/statusthree.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
    @elseif($formrequests->status->id == 4)
                @if($formrequests->fromwichstatus() == 1)
                <img src="{{url('img/statusonedenied.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
                @elseif($formrequests->fromwichstatus() == 2)
                <img src="{{url('img/statustwodenied.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
                @elseif($formrequests->fromwichstatus() == 3)
                <img src="{{url('img/statusthreedenied.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
                @endif
    @elseif($formrequests->status->id == 5)
    <img src="{{url('img/applied.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
    @endif
    <table class="table table-bordered table-striped" style="direction:rtl;">
        <tbody>
          <tr>
            <td style="text-align:right; text-decoration: underline;">שם המבקש: </td>
            <td>{{$formrequests->owner->name}} </td>
            <td style="text-align:right; text-decoration: underline;">מחלקה:</td>
            <td>{{$formrequests->owner->department->name}}</td>
          </tr>
          <tr>
            <td style="text-align:right; text-decoration: underline;">מעמד :</td>
            <td>{{$formrequests->owner->position->name}}  </td>
            <td style="text-align:right; text-decoration: underline;">נוצר בתאריך : </td>
            <td>{{$formrequests->created_at}}   </td>
          </tr>
          <tr>
            <td style="text-align:right; text-decoration: underline;"> פלאפון   :  </td>
            <td>{{$formrequests->owner->phone}} </td>
            <td style="text-align:right; text-decoration: underline;">כתובת מייל :   </td>
            <td>{{$formrequests->owner->email}}</td>
          </tr>
        </tbody>
      </table>
    <p class="card-text" style="text-align:right; text-decoration: underline;"><b>סטטוס בקשה :       </b></p>
    <p class="card-text" style="text-align:right">{{$formrequests->status->name}}     </p>
    <p class="card-text" style="text-align:right; text-decoration: underline;"><b>תיאור בקשת השינוי כולל מטרה וסיבת הבקשה:        </b></p>
    <p class="card-text" style="text-align:right">{{$formrequests->texta}}     </p>
    <p class="card-text" style="text-align:right; text-decoration: underline;"><b>האם יש צורך בתיעדוף השינוי בנסיבות מיוחדות אנא פרט:        </b></p>
    <p class="card-text" style="text-align:right">{{$formrequests->textb}}      </p>
    <p class="card-text" style="text-align:right; text-decoration: underline;"><b>האם מדובר בשינוי חוזר או ידוע למגיש הבקשה על שינוי חוזר:        </b></p>
    <p class="card-text" style="text-align:right">{{$formrequests->textc}}      </p>
     <p class="card-text" style="text-align:right; text-decoration: underline;"><b>האם ידוע למגיש הבקשה על מערכות אחרות שיושפעו מתהליך השינוי ?      </b></p>
     <p class="card-text" style="text-align:right">{{$formrequests->textd}}     </p>
    @if(@isset($formrequests->influencessystem))
    <p class="card-text" style="text-align:right"><b>: פירוט        </b></p>
    <p class="card-text" style="text-align:right">{{$formrequests->influencessystem}}     </p>
    @endisset
    <table class="table table-bordered table-striped" style="direction:rtl;">
        <tbody>
          <tr>
            <td style="text-align:right; text-decoration: underline;">הערות מנמ"ר: </td>
            <td>{{$statusIt}} </td>

          </tr>
          <tr>
            <td style="text-align:right; text-decoration: underline;">הערות סייבר:</td>
            <td>{{$statusCyber}}</td>
          </tr>
          <tr>
            <td style="text-align:right; text-decoration: underline;">הערות יישום:</td>
            <td>{{$statusApp}}</td>
          </tr>
        </tbody>
      </table>
@if(Auth::user()->isCyber()||Auth::user()->isIt()||Auth::user()->isAdmin())
<div class="card-footer" style="direction: ltr;">

<div style=" direction:rtl;">
    <form method = "post" action = "{{ action('FormrequestsController@statusC',$formrequests->id) }}">
        @csrf
        <div class="form-row align-items-center" >
          <div class="col-auto my-1">
            @if (App\Status::next($formrequests->status_id) != null )
            <select class="custom-select mr-sm-2 align-items-right" id="status_id" name="status_id">
                <option value={{$formrequests->status_id}} class="card-text" style="text-align:right">בחר סטטוס</option>
                @foreach(App\Status::next($formrequests->status_id) as $status)
              <option value={{$status->id}}>{{$status->name}}</option>
              @endforeach
            </select>
        </div>
                <div class="form-group col-md-10 mb-10">
                    <label for="rejected" class="card-text" style="text-align:right">פרט שינוי :</label>
                    <input type="text" class="form-control" id="rejected" name="rejected" placeholder="פרטי שינוי" required>
                </div>
            <div class="form-group col-auto my-1">
                <button type="submit" name = "submit" class="btn btn-primary" class="card-text">עדכן סטטוס בקשה</button>
              </div>
            @else
            <p class="card-text" style="text-align:right; text-decoration: underline;"><b>סטטוס בקשה :   </b></p>
            <p class="card-text" style="text-align:right"> {{$formrequests->status->name}}     </p>
            @endif
        </div>
      </form>
</div>
</div>
@endif
@endsection
