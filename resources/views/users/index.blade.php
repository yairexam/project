@extends('layouts.app')

@section('title', 'Users')

@section('content' )
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<div class="container" style="direction:rtl; text-align:right;">
<h1  style="text-align:right; background: rgba(255, 255, 255, 0.8);" >רשימת משתמשים במערכת</h1>
<div class="row">
    <div class="col-lg-4 col-6">
        <!-- small box -->
        <div class="small-box bg-light">

          <div class="inner">
            <h3>{{Auth::user()->countUser()}}<sup style="font-size: 20px"></sup></h3>

            <p>מספר משתמשים רשומים</p>
          </div>
        </div>
      </div>
      <!-- ./col -->
    <div class="col-lg-4 col-6">
      <!-- small box -->
      <div class="small-box bg-light">
        <div class="inner">
          <h3>{{Auth::user()->activeReqUser()}}</h3>

          <p>משתמשים עם בקשות פעילות</p>
        </div>
      </div>
    </div>
    <!-- ./col -->

    <div class="col-lg-4 col-6">
      <!-- small box -->
      <div class="small-box bg-light">
        <div class="inner">
          <h3>{{Auth::user()->activeUser()}}</h3>

          <p>משתמשים פעילים</p>
        </div>
      </div>
    </div>

  </div>

<!-- TO DO List -->
<div class="card bg-primary-gradient" style="text-align:right">
    <div class="card-header no-border">
      <h3 class="card-title">
        <i class="ion ion-clipboard mr-1"></i>
        משתמשים במערכת
      </h3>

      <div class="card-tools">
        <ul class="pagination pagination-sm">
          {{$users->links()}}
        </ul>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body " >
    <table class="table">
        <tr>
            <th>שם</th><th>מייל</th><th>מחלקה</th>
        </tr>

        @foreach($users as $user)

        <tr>
          <!-- drag handle -->

          <!-- checkbox -->

          <!-- todo text -->
          <td>{{$user->name}}</td>
          <td>{{$user->email}}</td>
          <td>{{$user->department->name}}</td>
        <td>
            <a href = "{{route('users.edit',$user->id)}}">ערוך פרטים</a>
        </td>
        <td>
                <a href = "{{route('user.delete',$user->id)}}">מחק משתמש</a>
        </td>
          {{-- <!-- General tools such as edit or delete-->
          <div class="tools">
            <i class="fa fa-edit"></i>
            <i class="fa fa-trash-o"></i>
          </div> --}}
        </tr>
        @endforeach
      </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection



