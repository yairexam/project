@extends('layouts.app')

@section('title', 'Edit user')

@section('content')
<p></p>
<div class="container contact col-md-6" style=" direction: rtl; text-align:right; background: rgba(255, 255, 255, 0.3); ">
    <div class="contact-form">
       <h1>ערוך פרופיל</h1>
        <form method = "post" action = "{{action('UsersController@update',$user->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group ">
            <label for = "name">שם משתמש</label>
            <div class="col-md-6">
            <input type = "text" class="form-control" name = "name" value = {{$user->name}}>
            </div>
        </div>
        <div class="form-group">
            <label for = "email">אימייל</label>
            <div class="col-md-6">
            <input type = "text" class="form-control" name = "email" value = {{$user->email}}>
            </div>
        </div>
        @if(Auth::user()->isAdmin())
        <div class="form-group">
            <label for="department_id" >מחלקה</label>
            <div class="col-md-6">
                <select class="form-control" name="department_id">
                    <option value="{{ $user->department->id }}">{{$user->department->name}}</option>
                   @foreach ($departments as $department)
                    @if($user->department_id != $department->id)
                        <option value="{{ $department->id }}">
                            {{ $department->name }}
                        </option>
                     @endif
                   @endforeach
                 </select>
            </div>
        </div>

        @if($user->hasrole() == false)
        <div class="form-group">
            <label for="role_id" >Role</label>
            <div class="col-md-6">
                <select class="form-control" id ="role_id" name="role_id">
                    <option value=""> אין הרשאה</option>
                   @foreach ($roles as $role)
                        <option value="{{ $role->id }}">
                            {{ $role->name }}
                        </option>
                   @endforeach
                 </select>
            </div>
        </div>
        @else
        <div class="col-md-6">
            <label for = "role_id">הרשאות משתמש :</label>
            <label class="form-control" for="role_id" name="role_id">{{$user->getrole()}}</label>
            <button type="button" class="btn btn-light" >
            <a href = "{{route('users.deletepremition',$user->id)}}" > מחק הרשאה</a>
            </button><div>
        @endif
        @endif
            </p>
        <div>
            <input class="btn btn-primary" type = "submit" name = "submit" value = "עדכן פרטים">
        </div>
        </form>
    </div>
</div>
@endsection
