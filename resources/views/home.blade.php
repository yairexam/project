@extends('layouts.app')

@section('content')
<div class="container" style="direction: rtl">
    <p></p>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" style="text-align:right;color:green; "><b>התחברת בהצלחה!</b></div>

                <div class="card-body">
                    <div class="card-header" style="text-align:right">תהליך אישור ויישום בקשה:</div>
                    <img src="{{url('img/proccess.png')}}" alt="image" style="max-width:140%; max-height:100%;"/><p></p>
                    <div style="text-align:right"><b>לתחילת התהליך מלא טופס בקשה:</b></div><p></p>
                    <a href='{{url('/formrequests/create')}}' ><button class="btn btn-primary btn-lg btn-block">פתח בקשה חדשה</button></a>
                </div>
            </div>
        </div>
</div>


@endsection
