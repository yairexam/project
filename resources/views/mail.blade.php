<div class="card text-center ">
    <div class="card-header" style="text-align:center">    <b>  מספר אסמכתא לבקשה:            </b>      {{$details->id}}
    </div>
    <div class="card-body">
    <!-- the table data -->
    @if($details->status->id == 1)
    <img src="{{url('img/statusone.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
    @elseif($details->status->id == 2)
    <img src="{{url('img/statustwo.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
    @elseif($details->status->id == 3)
    <img src="{{url('img/statusthree.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
    @elseif($details->status->id == 4)
                @if($details->fromwichstatus() == 1)
                <img src="{{url('img/statusonedenied.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
                @elseif($details->fromwichstatus() == 2)
                <img src="{{url('img/statustwodenied.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
                @elseif($details->fromwichstatus() == 3)
                <img src="{{url('img/statusthreedenied.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
                @endif
    @elseif($details->status->id == 5)
    <img src="{{url('img/applied.png')}}" alt="image" style="max-width:140%; max-height:100%;"/>
    @endif
    <table class="table table-bordered table-striped" style="direction:rtl;">
        <tbody>
          <tr>
            <td style="text-align:right; text-decoration: underline;">שם המבקש: </td>
            <td>{{$details->owner->name}} </td>
            <td style="text-align:right; text-decoration: underline;">מחלקה:</td>
            <td>{{$details->owner->department->name}}</td>
          </tr>
          <tr>
            <td style="text-align:right; text-decoration: underline;">מעמד :</td>
            <td>{{$details->owner->position->name}}  </td>
            <td style="text-align:right; text-decoration: underline;">נוצר בתאריך : </td>
            <td>{{$details->created_at}}   </td>
          </tr>
          <tr>
            <td style="text-align:right; text-decoration: underline;"> פלאפון   :  </td>
            <td>{{$details->owner->phone}} </td>
            <td style="text-align:right; text-decoration: underline;">כתובת מייל :   </td>
            <td>{{$details->owner->email}}</td>
          </tr>
        </tbody>
      </table>
    <p class="card-text" style="text-align:right; text-decoration: underline;"><b>סטטוס בקשה :       </b></p>
    <p class="card-text" style="text-align:right">{{$details->status->name}}     </p>
    <p class="card-text" style="text-align:right; text-decoration: underline;"><b>תיאור בקשת השינוי כולל מטרה וסיבת הבקשה:        </b></p>
    <p class="card-text" style="text-align:right">{{$details->texta}}     </p>
    <p class="card-text" style="text-align:right; text-decoration: underline;"><b>האם יש צורך בתיעדוף השינוי בנסיבות מיוחדות אנא פרט:        </b></p>
    <p class="card-text" style="text-align:right">{{$details->textb}}      </p>
    <p class="card-text" style="text-align:right; text-decoration: underline;"><b>האם מדובר בשינוי חוזר או ידוע למגיש הבקשה על שינוי חוזר:        </b></p>
    <p class="card-text" style="text-align:right">{{$details->textc}}      </p>
     <p class="card-text" style="text-align:right; text-decoration: underline;"><b>האם ידוע למגיש הבקשה על מערכות אחרות שיושפעו מתהליך השינוי ?      </b></p>
     <p class="card-text" style="text-align:right">{{$details->textd}}     </p>
    @if(@isset($details->influencessystem))
    <p class="card-text" style="text-align:right"><b>: פירוט        </b></p>
    <p class="card-text" style="text-align:right">{{$details->influencessystem}}     </p>
    @endisset
    </div>
