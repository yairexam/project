<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>מערכת שינויים המכון הגאולוגי</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            .background {
            border: 2px solid white;
            border-radius: 8px;
            background-color: white;
            color: #000000;
            opacity: 0.6;
            display: flex;
            text-align: center;
            font-size: 60px;
            }
            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 60px;
            }

            .links > a {
                border: 2px solid black;
                border-radius: 8px;
                background-color: black;
                opacity: 0.6;
                color: white;
                padding: 0 25px;
                font-size: 30px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .img {
                border-radius: 15px;
                position: absolute;
                top: 8px;
                left: 16px;

            }
            .footer {
                color: rgb(0, 0, 0);
                position: absolute;
                bottom: 8px;
                right: 16px;
                font-size: 30px;
            }
        </style>
    </head>
    <div style="background-image: url('{{ asset('img/background.png')}}');">
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="background"><p>
                    ברוכים הבאים למערכת בקשת שינויים במערכות מידע
                    </p></div>
                    <div class="links">
                    @if (Route::has('login'))
                    @auth
                    <button type="button" class="btn btn-outline-dark">
                        <a href="{{ url('/home') }}">בקשות שלי</a></button>
                    @else
                        <a href="{{ route('login') }}">התחבר</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">הרשם</a>
                        @endif
                    @endauth
            @endif
            </div></div>
            <div class = "img"> <img src = "{{url('img/logo.jpg')}}" width="150" height="150"></div>
        </div>
        </div>

    </body>
</div>
</html>
