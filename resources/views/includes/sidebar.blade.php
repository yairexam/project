
   <!-- Main Sidebar Container -->
  <aside class="sidenav sidebar-dark-primary " style=" width:17%;  ">
    <!-- Brand Logo -->

    <li class="nav-item">

        <a class="nav-link" href="{{ route('logout') }} " style="text-align:right;"
             onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                                 <i class="far fa-circle nav-icon">  התנתק   </i>
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
        </form>
    </li>
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('img/logo.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info" style="text-align:right;">
          <a href="{{ route('users.edit',Auth::user()->id) }}" class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div>
    <!-- Sidebar -->
    <div class="sidebar">
    @if (!Auth::guest())
     @if (Auth::user()->isCyber())
                 <!-- Sidebar Menu -->
      <nav class="mt-2">

        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

              <li class="nav-item">
              @if(Route::currentRouteName()=='formrequests.updatedrequests')
            <a href="{{ route('formrequests.updatedrequests') }}" class="nav-link active" style="text-align:right;">
                @else
                <a href="{{ route('formrequests.updatedrequests') }}" class="nav-link" style="text-align:right;">
                @endif
              <i class="nav-icon fas fa-th"></i>
              <p>
                עדכונים אישיים
                @if(Auth::user()->updatingg()!=0)
                <span class="right badge badge-danger" >!</span>
                @endif
              </p>
            </a>
          </li>
          @if(Route::currentRouteName()=='formrequests.cyberrequests' || Route::currentRouteName()=='formrequests.apprequests')
          <li class="nav-item has-treeview  menu-open" style="text-align:right;">
            @else
          <li class="nav-item has-treeview" style="text-align:right;">
            @endif
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                     בקשות פעילות
                     @if(Auth::user()->updatesCyber() || Auth::user()->updatesApp())
                <span class="right badge badge-danger" >!</span>
            @endif
                <i class="fas fa-angle-left left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                @if(Route::currentRouteName()=='formrequests.cyberrequests')
                <a href="{{ route('formrequests.cyberrequests') }}" class="nav-link active" style="text-align:right;">
                    @else
                <a href="{{ route('formrequests.cyberrequests') }}" class="nav-link">
                    @endif
                  <i class="far fa-circle nav-icon"></i>
                  <p> לאישור סייבר
                    @if(Auth::user()->updatesCyber())
                    <span class="right badge badge-danger" >!</span>
                    @endif
                  </p>
                </a>
              </li>

              <li class="nav-item">
                @if(Route::currentRouteName()=='formrequests.apprequests')
                <a href="{{ route('formrequests.apprequests') }}" class="nav-link active" style="text-align:right;">
                    @else
                <a href="{{ route('formrequests.apprequests') }}" class="nav-link">
                    @endif
                  <i class="far fa-circle nav-icon"></i>
                  <p>ממתינות יישום
                    @if(Auth::user()->updatesApp())
                    <span class="right badge badge-danger" >!</span>
                    @endif
                  </p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            @if(Route::currentRouteName()=='formrequests.create')
            <a href="{{url('/formrequests/create')}}" class="nav-link active" style="text-align:right;">
                @else
            <a href="{{url('/formrequests/create')}}" class="nav-link" style="text-align:right;">
                @endif
              <i class="nav-icon fas fa-book"></i>
              <p>
                יצירת בקשה חדשה
              </p>
            </a>

          </li>
          @if(Route::currentRouteName()=='formrequests.deniedrequests' || Route::currentRouteName()=='formrequests.aprovedrequests')
          <li class="nav-item has-treeview  menu-open" style="text-align:right;">
            @else
          <li class="nav-item has-treeview" style="text-align:right;">
            @endif
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-envelope"></i>
              <p>
                ארכיון
              </p>
              <i class="fas fa-angle-left left"></i>
            </a>
            <ul class="nav nav-treeview" >
              <li class="nav-item">
                @if(Route::currentRouteName()=='formrequests.deniedrequests')
                <a href="{{ route('formrequests.deniedrequests') }}" class="nav-link active" style="text-align:right;">
                    @else
                <a href="{{ route('formrequests.deniedrequests') }}" class="nav-link">
                    @endif
                  <i class="far fa-circle nav-icon"></i>
                  <p>בקשות שנדחו</p>
                </a>
              </li>
              <li class="nav-item">
                @if(Route::currentRouteName()=='formrequests.aprovedrequests')
                <a href="{{ route('formrequests.aprovedrequests') }}" class="nav-link active" style="text-align:right;">
                    @else
                <a href="{{ route('formrequests.aprovedrequests') }}" class="nav-link">
                    @endif
                  <i class="far fa-circle nav-icon"></i>
                  <p>בקשות שבוצעו</p>
                </a>
              </li>
            </ul>
          </li>
          @if(Route::currentRouteName()=='formrequests.myrequests' || Route::currentRouteName()=='users.edit')
          <li class="nav-item has-treeview  menu-open" style="text-align:right;">
            @else
            <li class="nav-item has-treeview" style="text-align:right;">
                @endif
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                אזור אישי
              </p>
              <i class="fas fa-angle-left left"></i>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    @if(Route::currentRouteName()=='formrequests.myrequests')
                <a href="{{ route('formrequests.myrequests') }}" class="nav-link active" style="text-align:right;">
                    @else
                    <a href="{{ route('formrequests.myrequests') }}" class="nav-link">
                        @endif
                      <i class="far fa-circle nav-icon"></i>
                      <p>בקשות שלי</p>
                    </a>
                  </li>
                <li class="nav-item">
                    @if(Route::currentRouteName()=='users.edit')
                    <a href="{{ route('users.edit',Auth::user()->id) }}" class="nav-link active" style="text-align:right;">
                    @else
                    <a href="{{ route('users.edit',Auth::user()->id) }}" class="nav-link">
                        @endif
                      <i class="far fa-circle nav-icon"></i>
                      <p>פרופיל</p>
                    </a>
                  </li>
               </ul>
          </li>
        </nav>
      <!-- /.sidebar-menu -->
                @endif
            @endif
            @if (!Auth::guest())
            @if (Auth::user()->isAdmin())
             <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
      <li class="nav-item has-treeview" style="text-align:right;">
        @if(Route::currentRouteName()=='formrequests.managment')
        <a href="{{ route('formrequests.managment') }}" class="nav-link active" style="text-align:right;">
            @else
        <a href="{{ route('formrequests.managment') }}" class="nav-link">
            @endif
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>
            לוח מעקב
          </p>
        </a>

      </li>
      <li class="nav-item">
        @if(Route::currentRouteName()=='formrequests.updatedrequests')
        <a href="{{ route('formrequests.updatedrequests') }}" class="nav-link active" style="text-align:right;">
            @else
        <a href="{{ route('formrequests.updatedrequests') }}" class="nav-link" style="text-align:right;">
            @endif
          <i class="nav-icon fas fa-th"></i>
          <p>
               עדכונים אישיים
            @if(Auth::user()->updatingg()!=0)
            <span class="right badge badge-danger" >!</span>
            @endif
          </p>
        </a>
      </li>
      <li class="nav-item">
        @if(Route::currentRouteName()=='formrequests.index')
        <a href="{{ route('formrequests.index') }}" class="nav-link active" style="text-align:right;">
            @else
        <a href="{{ route('formrequests.index') }}" class="nav-link" style="text-align:right;">
            @endif
          <i class="nav-icon fas fa-th"></i>
          <p>
            כל הבקשות
          </p>
        </a>
      </li>
      <li class="nav-item has-treeview" style="text-align:right;">
        @if(Route::currentRouteName()=='users.index')
        <a href="{{ route('users.index') }}" class="nav-link active" style="text-align:right;">
            @else
        <a href="{{ route('users.index') }}" class="nav-link">
            @endif
          <i class="nav-icon fas fa-edit"></i>
          <p>
                ניהול הרשאות
          </p>
        </a>
      </li>
      <li class="nav-item has-treeview">
        @if(Route::currentRouteName()=='formrequests.create')
        <a href="{{ route('formrequests.create') }}" class="nav-link active" style="text-align:right;">
            @else
        <a href="{{url('/formrequests/create')}}" class="nav-link" style="text-align:right;">
            @endif
          <i class="nav-icon fas fa-book"></i>
          <p>
            הוסף בקשה
          </p>
        </a>

      </li>
      @if(Route::currentRouteName()=='formrequests.deniedrequests' || Route::currentRouteName()=='formrequests.aprovedrequests')
          <li class="nav-item has-treeview  menu-open" style="text-align:right;">
            @else
      <li class="nav-item has-treeview" style="text-align:right;">
        @endif
        <a href="#" class="nav-link">
          <i class="nav-icon far fa-envelope"></i>
          <p>
            ארכיון
          </p>
          <i class="fas fa-angle-left left"></i>
        </a>
        <ul class="nav nav-treeview" >
          <li class="nav-item">
            @if(Route::currentRouteName()=='formrequests.deniedrequests')
            <a href="{{ route('formrequests.deniedrequests') }}" class="nav-link active" style="text-align:right;">
                @else
            <a href="{{ route('formrequests.deniedrequests') }}" class="nav-link">
                @endif
              <i class="far fa-circle nav-icon"></i>
              <p>בקשות שנדחו</p>
            </a>
          </li>
          <li class="nav-item">
            @if(Route::currentRouteName()=='formrequests.aprovedrequests')
            <a href="{{ route('formrequests.aprovedrequests') }}" class="nav-link active" style="text-align:right;">
                @else
            <a href="{{ route('formrequests.aprovedrequests') }}" class="nav-link">
                @endif
              <i class="far fa-circle nav-icon"></i>
              <p>בקשות שבוצעו</p>
            </a>
          </li>
        </ul>
      </li>
      @if(Route::currentRouteName()=='formrequests.myrequests' || Route::currentRouteName()=='users.edit')
      <li class="nav-item has-treeview  menu-open" style="text-align:right;">
        @else
      <li class="nav-item has-treeview" style="text-align:right;">
        @endif
        <a href="#" class="nav-link">
          <i class="nav-icon far fa-plus-square"></i>
          <p>
            אזור אישי
          </p>
          <i class="fas fa-angle-left left"></i>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                @if(Route::currentRouteName()=='formrequests.myrequests')
                <a href="{{ route('formrequests.myrequests') }}" class="nav-link active" style="text-align:right;">
                    @else
                <a href="{{ route('formrequests.myrequests') }}" class="nav-link">
                    @endif
                  <i class="far fa-circle nav-icon"></i>
                  <p>בקשות שלי</p>
                </a>
              </li>
            <li class="nav-item">
                @if(Route::currentRouteName()=='users.edit')
                <a href="{{ route('users.edit',Auth::user()->id)  }}" class="nav-link active" style="text-align:right;">
                    @else
                <a href="{{ route('users.edit',Auth::user()->id) }}" class="nav-link">
                    @endif
                  <i class="far fa-circle nav-icon"></i>
                  <p>פרופיל</p>
                </a>
              </li>
           </ul>
      </li>
    </nav>
  <!-- /.sidebar-menu -->
            @endif
        @endif
        @if (!Auth::guest())
            @if (Auth::user()->isIt())
             <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
      <li class="nav-item">
        @if(Route::currentRouteName()=='formrequests.updatedrequests')
        <a href="{{ route('formrequests.updatedrequests') }}" class="nav-link active" style="text-align:right;">
            @else
        <a href="{{ route('formrequests.updatedrequests') }}" class="nav-link" style="text-align:right;">
          <i class="nav-icon fas fa-th"></i>
          @endif
          <p>
            עדכונים אישיים
            @if(Auth::user()->updatingg()!=0)
            <span class="right badge badge-danger" >!</span>
            @endif
          </p>
        </a>
      </li>
      @if(Route::currentRouteName()=='formrequests.itrequests')
      <li class="nav-item has-treeview  menu-open" style="text-align:right;">
        @else
      <li class="nav-item has-treeview" style="text-align:right;">
        @endif
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-edit"></i>
          <p>
            @if(Auth::user()->updatesIt())
            <span class="right badge badge-danger" >!</span>
            @endif
                 בקשות ממתינות
            <i class="fas fa-angle-left left"></i>

          </p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                @if(Route::currentRouteName()=='formrequests.itrequests')
                <a href="{{ route('formrequests.itrequests') }}" class="nav-link active" style="text-align:right;">
                    @else
                <a href="{{ route('formrequests.itrequests') }}" class="nav-link">
                    @endif
                  <i class="far fa-circle nav-icon"></i>
                  <p>אישור מנמ"ר
                    @if(Auth::user()->updatesIt())
                    <span class="right badge badge-danger" >!</span>
                    @endif
                  </p>
                </a>      </li>
        </ul>
      </li>
      <li class="nav-item has-treeview">
        @if(Route::currentRouteName()=='formrequests.create')
        <a href="{{ route('formrequests.create') }}" class="nav-link active" style="text-align:right;">
            @else
        <a href="{{url('/formrequests/create')}}" class="nav-link" style="text-align:right;">
            @endif
          <i class="nav-icon fas fa-book"></i>
          <p>
            הוסף בקשה
          </p>
        </a>

      </li>
      @if(Route::currentRouteName()=='formrequests.deniedrequests' || Route::currentRouteName()=='formrequests.aprovedrequests')
      <li class="nav-item has-treeview  menu-open" style="text-align:right;">
        @else
      <li class="nav-item has-treeview" style="text-align:right;">
        @endif
        <a href="#" class="nav-link">
          <i class="nav-icon far fa-envelope"></i>
          <p>
            ארכיון
          </p>
          <i class="fas fa-angle-left left"></i>
        </a>
        <ul class="nav nav-treeview" >
          <li class="nav-item">
            @if(Route::currentRouteName()=='formrequests.deniedrequests')
            <a href="{{ route('formrequests.deniedrequests') }}" class="nav-link active" style="text-align:right;">
                @else
            <a href="{{ route('formrequests.deniedrequests') }}" class="nav-link">
                @endif
              <i class="far fa-circle nav-icon"></i>
              <p>בקשות שנדחו</p>
            </a>
          </li>
          <li class="nav-item">
            @if(Route::currentRouteName()=='formrequests.aprovedrequests')
            <a href="{{ route('formrequests.aprovedrequests') }}" class="nav-link active" style="text-align:right;">
                @else
            <a href="{{ route('formrequests.aprovedrequests') }}" class="nav-link">
                @endif
              <i class="far fa-circle nav-icon"></i>
              <p>בקשות שבוצעו</p>
            </a>
          </li>
        </ul>
      </li>
      @if(Route::currentRouteName()=='formrequests.myrequests' || Route::currentRouteName()=='users.edit')
      <li class="nav-item has-treeview  menu-open" style="text-align:right;">
        @else
      <li class="nav-item has-treeview" style="text-align:right;">
        @endif
        <a href="#" class="nav-link">
          <i class="nav-icon far fa-plus-square"></i>
          <p>
            אזור אישי
          </p>
          <i class="fas fa-angle-left left"></i>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                @if(Route::currentRouteName()=='formrequests.myrequests')
                <a href="{{ route('formrequests.myrequests') }}" class="nav-link active" style="text-align:right;">
                    @else
                <a href="{{ route('formrequests.myrequests') }}" class="nav-link">
                    @endif
                  <i class="far fa-circle nav-icon"></i>
                  <p>בקשות שלי</p>
                </a>
              </li>
            <li class="nav-item">
                @if(Route::currentRouteName()=='users.edit')
                <a href="{{ route('users.edit',Auth::user()->id) }}" class="nav-link active" style="text-align:right;">
                    @else
                <a href="{{ route('users.edit',Auth::user()->id) }}" class="nav-link">
                    @endif
                  <i class="far fa-circle nav-icon"></i>
                  <p>פרופיל</p>
                </a>
              </li>
           </ul>
      </li>

  <!-- /.sidebar-menu -->
            @endif
        @endif
    </nav>

    @if (!Auth::guest())
                @if (!(Auth::user()->isCyber()) and !(Auth::user()->isIt()) and !(Auth::user()->isAdmin()))
                 <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            @if(Route::currentRouteName()=='formrequests.updatedrequests')
            <a href="{{ route('formrequests.updatedrequests') }}" class="nav-link active" style="text-align:right;">
                @else
            <a href="{{ route('formrequests.updatedrequests') }}" class="nav-link" style="text-align:right;">
                @endif
              <i class="nav-icon fas fa-th"></i>
              <p>
                עדכונים אישיים
                @if(Auth::user()->updatingg()!=0)
                <span class="right badge badge-danger" >!</span>
                @endif
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview">
            @if(Route::currentRouteName()=='formrequests.create')
            <a href="{{ route('formrequests.create') }}" class="nav-link active" style="text-align:right;">
                @else
            <a href="{{url('/formrequests/create')}}" class="nav-link" style="text-align:right;">
                @endif
              <i class="nav-icon fas fa-book"></i>
              <p>
                הוסף בקשה
              </p>
            </a>

          </li>
          <li class="nav-item" style="text-align:right;">
            @if(Route::currentRouteName()=='formrequests.myrequests')
            <a href="{{ route('formrequests.myrequests') }}" class="nav-link active" style="text-align:right;">
                @else
            <a href="{{ route('formrequests.myrequests') }}" class="nav-link">
                @endif
              <i class="far fa-circle nav-icon"></i>
              <p>בקשות שלי</p>
            </a>
          </li>
          @if(Route::currentRouteName()=='formrequests.deniedrequests' || Route::currentRouteName()=='formrequests.aprovedrequests')
          <li class="nav-item has-treeview  menu-open" style="text-align:right;">
            @else
          <li class="nav-item has-treeview" style="text-align:right;">
            @endif
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-envelope"></i>
              <p>
                ארכיון
              </p>
              <i class="fas fa-angle-left left"></i>
            </a>
            <ul class="nav nav-treeview" >
              <li class="nav-item">
                @if(Route::currentRouteName()=='formrequests.deniedrequests')
                <a href="{{ route('formrequests.deniedrequests') }}" class="nav-link active" style="text-align:right;">
                    @else
                <a href="{{ route('formrequests.deniedrequests') }}" class="nav-link">
                    @endif
                  <i class="far fa-circle nav-icon"></i>
                  <p>בקשות שנדחו</p>
                </a>
              </li>
              <li class="nav-item">
                @if(Route::currentRouteName()=='formrequests.aprovedrequests')
                <a href="{{ route('formrequests.aprovedrequests') }}" class="nav-link active" style="text-align:right;">
                    @else
                <a href="{{ route('formrequests.aprovedrequests') }}" class="nav-link">
                    @endif
                  <i class="far fa-circle nav-icon"></i>
                  <p>בקשות שבוצעו</p>
                </a>
              </li>
            </ul>
          </li>
          @if(Route::currentRouteName()=='users.edit' )
          <li class="nav-item has-treeview  menu-open" style="text-align:right;">
            @else
          <li class="nav-item has-treeview" style="text-align:right;">
            @endif
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                אזור אישי
              </p>
              <i class="fas fa-angle-left left"></i>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    @if(Route::currentRouteName()=='users.edit')
                <a href="{{ route('users.edit',Auth::user()->id) }}" class="nav-link active" style="text-align:right;">
                    @else
                    <a href="{{ route('users.edit',Auth::user()->id) }}" class="nav-link">
                        @endif
                      <i class="far fa-circle nav-icon"></i>
                      <p>פרופיל</p>
                    </a>
                  </li>
               </ul>
          </li>
        </nav>
                @endif
            @endif
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
