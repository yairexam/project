<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        @if (!Auth::guest())
        @if (Auth::user()->isAdmin()||Auth::user()->isIt()||Auth::user()->isCyber())
        <a class="navbar-brand" href="{{ url('/formrequests') }}">
                בקשות שינוי במערכת
        </a>
        @endif
        @endif

        @if (!Auth::guest())
        @if (Auth::user()->isIt())
        <a class="navbar-brand" href="{{ route('formrequests.itrequests') }}">
         בקשות ממתינות לאישור מנמ"ר
        </a>
        @endif
        @endif

        @if (!Auth::guest())
        @if (Auth::user()->isCyber())
        <a class="navbar-brand" href="{{ route('formrequests.cyberrequests') }}">
            בקשות ממתינות לאישור סייבר
        </a>
        @endif
        @endif

        @if (!Auth::guest())
        @if (Auth::user()->isCyber())
        <a class="navbar-brand" href="{{ route('formrequests.apprequests') }}">
                בקשות ממתינות ליישום
        </a>
        @endif
        @endif

        <a class="navbar-brand" href="{{ route('formrequests.myrequests') }}">
           בקשות השינוי שלי
         </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('התחבר') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('הרשמה') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
