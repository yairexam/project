<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('users', 'UsersController')->middleware('auth');
Route::resource('formrequests', 'FormrequestsController')->middleware('auth');
Route::get('formrequests/changeuser/{cid}/{uid?}', 'FormrequestsController@changeUser')->name('formrequests.changeuser')->middleware('auth');
Route::get('formrequests/changestatus/{cid}/{sid}', 'FormrequestsController@changeStatus')->name('formrequests.changestatus')->middleware('auth');

Route::get('myformrequests', 'FormrequestsController@myRequests')->name('formrequests.myrequests')->middleware('auth');
Route::get('myformrequests/Page/{id}', 'FormrequestsController@page')->name('formrequests.page')->middleware('auth');
Route::get('managment/', 'FormrequestsController@managment')->name('formrequests.managment')->middleware('auth');

Route::get('itformrequests', 'FormrequestsController@itRequests')->name('formrequests.itrequests')->middleware('auth');
Route::get('cyberformrequests', 'FormrequestsController@cyberRequests')->name('formrequests.cyberrequests')->middleware('auth');
Route::get('appformrequests', 'FormrequestsController@appRequests')->name('formrequests.apprequests')->middleware('auth');
Route::get('deniedformrequests', 'FormrequestsController@deniedRequests')->name('formrequests.deniedrequests')->middleware('auth');
Route::get('aprovedformrequests', 'FormrequestsController@aprovedRequests')->name('formrequests.aprovedrequests')->middleware('auth');
Route::get('updatedformrequests', 'FormrequestsController@updatedRequests')->name('formrequests.updatedrequests')->middleware('auth');
Route::get('user/deletepremition/{id}', 'UsersController@deletepremition')->name('users.deletepremition')->middleware('auth');
Route::post('myformrequests/statusC/{cid}', 'FormrequestsController@statusC')->name('formrequests.statusc')->middleware('auth');
Route::post('searchformbyid', 'FormrequestsController@searchformbyid')->name('formrequests.searchformbyid')->middleware('auth');
Route::post('searchformbystatus', 'FormrequestsController@searchformbystatus')->name('formrequests.searchformbystatus')->middleware('auth');
Route::post('searchformbychange', 'FormrequestsController@searchformbychange')->name('formrequests.searchformbychange')->middleware('auth');
Route::get('formrequests/delete/{id}', 'FormrequestsController@destroy')->name('formrequests.delete')->middleware('auth');
Route::get('user/delete/{id}', 'UsersController@destroy')->name('user.delete')->middleware('auth');

Route::post('multiplerecordsdelete', 'FormrequestsController@multiplerecordsdelete')->name('formrequests.multiplerecordsdelete')->middleware('auth');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
